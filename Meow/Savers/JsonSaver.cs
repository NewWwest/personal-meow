﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Meow.Models;

namespace Meow.Savers
{
    class JsonSaver : ISaver
    {
        public void Save(IEnumerable<ClassifiedImage> images)
        {
            using (var file = new FileStream("CatsSerialized/HelloThere.json", FileMode.Create))
            using (var writer = new StreamWriter(file))
            {
                writer.WriteLine("{");
                writer.WriteLine("'images': [");
                foreach (var image in images)
                {
                    Console.WriteLine("Writing new Image");
                    WriteImage(image, writer);
                    writer.Write(",");
                    writer.WriteLine();
                }
                writer.WriteLine("]");
                writer.WriteLine("}");
            }
        }

        private void WriteImage(ClassifiedImage image, StreamWriter writer)
        {
            writer.WriteLine("'image':");

            writer.Write("[");
            for (int y = 0; y < image.Height; y++)
            {
                writer.Write("[");
                for (int x = 0; x < image.Width; x++)
                {
                    writer.Write($"{image.Values[x, y]}, ");


                }
                writer.Write(image.Values[image.Width - 1, y]);
                writer.Write("],");
                writer.WriteLine();
            }
            writer.WriteLine("]");
            
        }
    }
}
