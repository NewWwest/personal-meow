﻿using Meow.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Meow.Savers
{
    interface ISaver
    {
        void Save(IEnumerable<ClassifiedImage> images); 
    }
}
