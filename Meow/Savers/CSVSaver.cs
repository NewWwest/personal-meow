﻿using Meow.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;

namespace Meow.Savers
{
    class CSVSaver : ISaver
    {
        public void Save(ClassifiedImage image, string filename, string path)
        {
            StringBuilder output = new StringBuilder(image.Values.Length * 5);
            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width - 1; j++)
                {
                    output.Append((float)image.Values[j, i]);
                    output.Append(", ");
                }

                output.Append(image.Values[image.Width - 1, i]);
                output.AppendLine();
            }
            File.WriteAllText(path + filename, output.ToString());

        }

        public void Save(IEnumerable<ClassifiedImage> images)
        {
            int i = 0;
            foreach (var image in images)
            {
                i++;
                Console.WriteLine($"Saving serialized image {image.Title} as a {image.animal}");
                Save(image, $"file{i}", $"{image.animal}sSerialized/");
            }
        }
    }
}
