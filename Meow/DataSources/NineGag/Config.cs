﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meow.DataSources.NineGag
{
    static class Config
    {
        static public readonly string[] catKeys = new string[] 
        {
            "whiskers",
            "cat",
            "purr",
            "kitten",
            "pussy",
            "meow",

        };
        static public readonly string[] dogKeys = new string[]
        {
            "dog",
            "puppy",
            "boi",
            "boye",
            "boy"

        };
        static private readonly string cuteBaseUrl = @"https://9gag.com/v1/group-posts/group/cute/type/hot";
        static private readonly string HotbaseUrl = @"https://9gag.com/v1/group-posts/group/default/type/hot";

        static public string Url => cuteBaseUrl;
        static public readonly string RandomId = @"";
    }
}
