﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meow.DataSources.NineGag
{
    public class Meta
    {
        public int timestamp { get; set; }
        public string status { get; set; }
        public string sid { get; set; }
    }

    public class Image700
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
        public string webpUrl { get; set; }
    }

    public class Image460
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
        public string webpUrl { get; set; }
    }

    public class Image460sv
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
        public int hasAudio { get; set; }
        public int duration { get; set; }
        public string h265Url { get; set; }
        public string vp9Url { get; set; }
    }

    public class Image460svwm
    {
        public int width { get; set; }
        public int height { get; set; }
        public string url { get; set; }
        public int hasAudio { get; set; }
        public int duration { get; set; }
    }

    public class Images
    {
        public Image700 image700 { get; set; }
        public Image460 image460 { get; set; }
        public Image460sv image460sv { get; set; }
        public Image460svwm image460svwm { get; set; }
    }

    public class Post
    {
        public string id { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string type { get; set; }
        public int nsfw { get; set; }
        public int upVoteCount { get; set; }
        public int promoted { get; set; }
        public int isVoteMasked { get; set; }
        public int hasLongPostCover { get; set; }
        public Images images { get; set; }
        public string sourceDomain { get; set; }
        public string sourceUrl { get; set; }
        public int commentsCount { get; set; }
        public List<string> sections { get; set; }
        public List<Tag> tags { get; set; }
        public string descriptionHtml { get; set; }
    }
    public class Tag
    {
        public string key { get; set; }
        public string url { get; set; }
    }
    public class Data
    {
        public List<Post> posts { get; set; }
        public string nextCursor { get; set; }
    }

    public class RootObject
    {
        public Meta meta { get; set; }
        public Data data { get; set; }
    }
}