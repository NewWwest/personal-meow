﻿using Meow.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Drawing;

namespace Meow.DataSources.NineGag
{
    class NineGag : DataSource
    {

        const int nOfIterations = 100;
        const int GetHeadersDelay = 1500;
        const int RequestDelay = 100;

        private IEnumerator<ClassifiedImage> GetData()
        {
            Console.WriteLine("NineGag Downloading Data...");
            using (var client = new WebClient())
            {
                string currentId = Config.RandomId;
                for (int i = 0; i < nOfIterations; i++)
                {
                    Console.WriteLine($"Start-Batch{i}:");
                    string fullUrl = $"{Config.Url}?after={currentId}&c=40";
                    var response = client.DownloadData(fullUrl);
                    var resp = Encoding.ASCII.GetString(response, 0, response.Length);

                    var jObject = Newtonsoft.Json.Linq.JObject.Parse(resp);
                    var data = jObject.ToObject<RootObject>();
                    currentId = data.data.nextCursor;
                    foreach (var post in data.data.posts)
                    {
                        ClassifiedImage currentImage = null;
                        try
                        {
                            currentImage = ProcessPost(post, client);
                            Thread.Sleep(RequestDelay);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine();
                            Console.WriteLine($"{post.title} failed");
                            Console.WriteLine(e.Message);
                            Console.WriteLine();
                        }
                        if (currentImage == null)
                            continue;
                        else
                            yield return currentImage;
                    }
                    Console.WriteLine($"End__-Batch{i}:");
                    Thread.Sleep(GetHeadersDelay);
                }
                yield break;
            }
        }

        private ClassifiedImage ProcessPost(Post post, WebClient client)
        {
            if (post.images.image460 != null)
            {
                string accumulated = post.title;
                foreach (var tag in post.tags)
                    accumulated = accumulated + " " + tag.key;
                accumulated=accumulated.ToLower();

                string safeTitle = post.title.Substring(0, Math.Min(post.title.Length, 20));
                foreach (var c in invalidFilenameChars)
                    safeTitle = safeTitle.Replace(c, '_');

                Console.WriteLine($"{ safeTitle} - {post.images.image460.url}");
                var JPEGData = client.DownloadData(post.images.image460.url);
                Image image = Image.FromStream(new MemoryStream(JPEGData));
                var bitmap = new Bitmap(image);
                var processed = ProcessImage(bitmap);
                processed.Title = safeTitle;

                if (Config.catKeys.Any(key => accumulated.Contains(key)))
                {
                    processed.animal = Animal.Cat;
                    File.WriteAllBytes($"Cats/{safeTitle}.jpg", JPEGData);
                    return processed;
                }
                if (Config.dogKeys.Any(key => accumulated.Contains(key)))
                {
                    processed.animal = Animal.Dog;
                    File.WriteAllBytes($"Dogs/{safeTitle}.jpg", JPEGData);
                    return processed;
                }
                processed.animal = Animal.None;
                File.WriteAllBytes($"None/{safeTitle}.jpg", JPEGData);
                return processed;
            }
            return null;
        }


        ClassifiedImage ProcessImage(Bitmap bitmap)
        {
            ClassifiedImage image = new ClassifiedImage()
            {
                Height = bitmap.Height,
                Width = bitmap.Width,
                Values = new int[bitmap.Width, bitmap.Height]
            };
            for (int i = 0; i < bitmap.Height; i++)
            {
                for (int j = 0; j < bitmap.Width; j++)
                {
                    image.Values[j,i] = (bitmap.GetPixel(j, i).G + bitmap.GetPixel(j, i).B + bitmap.GetPixel(j, i).R) / 3;
                }
            }
            return image;
        }

        public override IEnumerator<ClassifiedImage> GetEnumerator()
        {
            return GetData();
        }
    }
}
