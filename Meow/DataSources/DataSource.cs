﻿using Meow.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Meow.DataSources
{
    abstract class DataSource : IEnumerable<ClassifiedImage>
    {
        public readonly char[] invalidFilenameChars = Path.GetInvalidFileNameChars();

        abstract public IEnumerator<ClassifiedImage> GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
