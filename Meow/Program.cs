﻿using Meow.DataSources.NineGag;
using Meow.Models;
using Meow.Savers;
using System;
using System.Collections.Generic;

namespace Meow
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            IEnumerable<ClassifiedImage> NineGagSource = new NineGag();
            var CSVSaver = new CSVSaver();
            CSVSaver.Save(NineGagSource);
           
            Console.ReadLine();
        }
    }
}
