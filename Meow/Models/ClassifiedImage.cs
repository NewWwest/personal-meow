﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Meow.Models
{
    class ClassifiedImage
    {
        public Animal animal;
        public int[,] Values;
        public int Height;
        public int Width;
        public string Title;
    }
}
